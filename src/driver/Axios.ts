import { HttpClientHeaders, IHttpClient, IHttpRequestConfig } from '../module';
import axios, { AxiosInstance, AxiosError } from 'axios';
import { AppError } from '../module/common';

export abstract class Axios implements IHttpClient {
  constructor(baseURL: string, timeout: number, initialHeader: HttpClientHeaders) {
    const { Authorization, 'Content-Type': ContentType } = initialHeader;
    this.axiosInstance = axios.create({
      baseURL,
      timeout,
      headers: {
        Authorization,
        'Content-Type': ContentType,
      },
    });
  }
  private axiosInstance!: AxiosInstance;

  handleError<T>(error: Error): T {
    return new AppError({
      code: 500,
      message: error.message,
    }) as T;
  }

  handleErrorRequest<T>(error: unknown): T {
    if (axios.isAxiosError(error)) {
      return this.handleAxiosError<T>(error);
    } else {
      return this.handleError<T>(error as Error);
    }
  }

  abstract handleAxiosError<T>(error: AxiosError): T;

  async get<T>(requestConfig: IHttpRequestConfig<void>): Promise<T> {
    const { url } = requestConfig;
    try {
      const response = await this.axiosInstance.get(url);
      return response.data as T;
    } catch (error) {
      return this.handleErrorRequest(error);
    }
  }

  async post<X, T>(requestConfig: IHttpRequestConfig<X>): Promise<T> {
    const { url, data } = requestConfig;
    try {
      const response = await this.axiosInstance.post(url, data);
      return response.data as T;
    } catch (error) {
      return this.handleErrorRequest(error);
    }
  }

  async put<X, T>(requestConfig: IHttpRequestConfig<X>): Promise<T> {
    const { url, data } = requestConfig;
    try {
      const response = await this.axiosInstance.put(url, data);
      return response.data as T;
    } catch (error) {
      return this.handleErrorRequest(error);
    }
  }

  async delete<T>(requestConfig: IHttpRequestConfig<void>): Promise<T> {
    const { url } = requestConfig;
    try {
      const response = await this.axiosInstance.delete(url);
      return response.data as T;
    } catch (error) {
      return this.handleErrorRequest(error);
    }
  }
}
