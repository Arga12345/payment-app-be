import { AxiosError } from 'axios';
import { Axios } from './Axios';
import { AppError } from '../module/common';
import { IMidtransError } from '../module/common/dto';

export class MidtransClient extends Axios {
  constructor() {
    super(MidtransClient.defineHost(), 10000, {
      Authorization: MidtransClient.defineAuthorization(),
    });
  }

  static defineHost(): string {
    const host = process.env.MIDTRANS_HOST;
    return host ? host : '';
  }

  static defineAuthorization(): string {
    const serverKey = process.env.MIDTRANS_SERVER_KEY;
    return `Basic ${btoa(serverKey ? serverKey : '')}`;
  }

  handleAxiosError<T>(error: AxiosError<IMidtransError>): T {
    const { code, response, message } = error;
    if (response) {
      const { status_code, status_message } = response.data;
      return new AppError({
        code: Number(status_code),
        message: status_message,
      }) as T;
    } else {
      return new AppError({
        code: code ? Number(code) : 500,
        message: message ? message : 'Internal server error',
      }) as T;
    }
  }
}
