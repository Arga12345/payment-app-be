import express, { Application, Router } from 'express';
import { IServer } from '../../module';

export class ExpressApp implements IServer {
  constructor(
    private port: number,
    private routes: Router[],
  ) {
    this.app = express();
    this.configureMiddleware();
    this.configureRoutes();
  }

  private app!: Application;

  configureMiddleware(): void {
    this.app.use(express.json());
  }

  configureRoutes(): void {
    this.routes.forEach((el) => {
      this.app.use(el);
    });
  }

  startApp(): void {
    this.app.listen(this.port, () => {
      console.log(`Server is listening on port ${this.port}`);
    });
  }
}
