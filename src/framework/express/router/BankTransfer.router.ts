import { Request, Response, Router } from 'express';
import { IBankTransferController } from '../../../module/bank-transfer';
import { IAppError } from '../../../module';

const bankTransferRoute: (controller: IBankTransferController) => Router = (
  controller: IBankTransferController,
) => {
  const router = Router();
  router.post('/bill-payment', (req: Request, res: Response) => {
    controller
      .billPayment(req.body)
      .then((response) => {
        res.status(response.code).send(response);
      })
      .catch((err: IAppError) => {
        res.status(err.code).send(err);
      });
  });
  router.post('/va-payment', (req: Request, res: Response) => {
    controller
      .vaPayment(req.body)
      .then((response) => {
        res.status(response.code).send(response);
      })
      .catch((err: IAppError) => {
        res.status(err.code).send(err);
      });
  });
  return router;
};

export default bankTransferRoute;
