import { ExpressApp } from './framework';

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

const app = new ExpressApp(3000, []);

app.startApp();
