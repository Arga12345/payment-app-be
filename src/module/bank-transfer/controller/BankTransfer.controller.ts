import { AppErrorUnprocessableEntity, AppResponse, IAppResponse } from '../../common';
import { IBillPayRequestDto, IVaRequestDto } from '../dto';
import { IBillPayRes, IVaRes } from '../entity';
import { ICreateBillPayTransactionUC, ICreateVaTransactionUC } from '../usecase';

export interface IBankTransferController {
  billPayment(data: IBillPayRequestDto): Promise<IAppResponse<IBillPayRes>>;
  vaPayment(data: IVaRequestDto): Promise<IAppResponse<IVaRes>>;
}

export class BankTransferController implements IBankTransferController {
  constructor(
    private x: ICreateBillPayTransactionUC,
    private y: ICreateVaTransactionUC,
  ) {}
  billPayment(data: IBillPayRequestDto): Promise<IAppResponse<IBillPayRes>> {
    return new Promise((resolve, reject) => {
      try {
        const { amount, userId } = data;
        this.x
          .execute(amount as number, userId as string)
          .then((response) => {
            resolve(
              new AppResponse({
                data: response,
                code: 201,
              }),
            );
          })
          .catch((err) => {
            reject(err);
          });
      } catch (error) {
        reject(new AppErrorUnprocessableEntity([(error as Error).message]));
      }
    });
  }

  vaPayment(data: IVaRequestDto): Promise<IAppResponse<IVaRes>> {
    return new Promise((resolve, reject) => {
      try {
        const { amount, userId, bank } = data;
        this.y
          .execute(bank, amount, userId)
          .then((response) => {
            resolve(
              new AppResponse({
                data: response,
                code: 201,
              }),
            );
          })
          .catch((err) => {
            reject(err);
          });
      } catch (error) {
        reject(new AppErrorUnprocessableEntity([(error as Error).message]));
      }
    });
  }
}
