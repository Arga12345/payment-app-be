export interface IBillPayRequestDto {
  amount: number;
  userId: string;
}
