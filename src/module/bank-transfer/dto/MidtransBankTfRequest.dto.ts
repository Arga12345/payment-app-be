import { BankTransfer } from '../entity';

export type IBankTfPaymentType = 'bank_transfer' | 'echannel' | 'permata';

export interface IBankTfTransactionDetail {
  order_id: string;
  gross_amount: number;
}

export interface IBankTransfer {
  bank: BankTransfer;
}

export interface IBankTfEchannel {
  bill_info1: string;
  bill_info2: string;
  bill_key?: string;
}
