import {
  IBankTfEchannel,
  IBankTfPaymentType,
  IBankTfTransactionDetail,
} from './MidtransBankTfRequest.dto';

export interface IMidtransBillPayRequestDto {
  payment_type: IBankTfPaymentType;
  transaction_details: IBankTfTransactionDetail;
  echannel: IBankTfEchannel;
}

export class MidtransBillPayRequestDto implements IMidtransBillPayRequestDto {
  constructor(data: IMidtransBillPayRequestDto) {
    Object.assign(this, data);
  }
  payment_type!: IBankTfPaymentType;
  transaction_details!: IBankTfTransactionDetail;
  echannel!: IBankTfEchannel;
}
