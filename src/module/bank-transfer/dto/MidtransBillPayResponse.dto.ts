import { IMidtransBankTfResponseDto } from './MidtransBankTfResponse.dto';

export interface IMidtransBillPayResponseDto extends IMidtransBankTfResponseDto {
  bill_key: string;
  biller_code: string;
}
