export interface IMidtransVaNumberDto {
  bank: string;
  va_number: string;
}
