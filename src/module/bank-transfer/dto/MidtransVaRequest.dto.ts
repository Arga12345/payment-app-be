import {
  IBankTfPaymentType,
  IBankTfTransactionDetail,
  IBankTransfer,
} from './MidtransBankTfRequest.dto';

export interface IMidtransVaRequestDto {
  payment_type: IBankTfPaymentType;
  transaction_details: IBankTfTransactionDetail;
  bank_transfer: IBankTransfer;
}

export class MidtransVaRequestDto implements IMidtransVaRequestDto {
  constructor(data: IMidtransVaRequestDto) {
    Object.assign(this, data);
  }
  payment_type!: IBankTfPaymentType;
  transaction_details!: IBankTfTransactionDetail;
  bank_transfer!: IBankTransfer;
}
