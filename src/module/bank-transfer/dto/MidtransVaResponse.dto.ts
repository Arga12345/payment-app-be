import { IMidtransBankTfResponseDto } from './MidtransBankTfResponse.dto';
import { IMidtransVaNumberDto } from './MidtransVaNumber.dto';

export interface IMidtransVaResponseDto extends IMidtransBankTfResponseDto {
  va_numbers: IMidtransVaNumberDto[];
}

export interface IMidtransPermataVaResponseDto extends IMidtransBankTfResponseDto {
  permata_va_number: string;
}
