import { BankTransfer } from '../entity';

export interface IVaRequestDto {
  amount: number;
  userId: string;
  bank: BankTransfer;
}
