export interface IBillPayReq {
  orderId: string;
  amount: number;
}
