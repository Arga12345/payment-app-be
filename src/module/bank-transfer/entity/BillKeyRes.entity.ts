export interface IBillPayRes {
  billKey: string;
  billerCode: string;
  transactionStatus: string;
  orderId: string;
}
