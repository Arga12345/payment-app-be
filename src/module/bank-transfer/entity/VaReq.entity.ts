import { BankTransfer } from './BankTransfer.entity';

export interface IVaReq {
  orderId: string;
  amount: number;
  bank: BankTransfer;
}
