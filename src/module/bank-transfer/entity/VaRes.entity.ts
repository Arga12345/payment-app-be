export interface IVaRes {
  vaNumber: string;
  transactionStatus: string;
  orderId: string;
}
