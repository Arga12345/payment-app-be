import { IBillPayReq, IBillPayRes } from '../entity';

export interface IBillPayRepository {
  createTransaction(request: IBillPayReq): Promise<IBillPayRes>;
}
