import { IVaReq, IVaRes } from '../entity';

export interface IVaRepository {
  createTransaction(data: IVaReq): Promise<IVaRes>;
}
