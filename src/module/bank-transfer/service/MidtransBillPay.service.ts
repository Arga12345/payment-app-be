import { IHttpClient } from '../../http-client';
import {
  IMidtransBillPayRequestDto,
  IMidtransBillPayResponseDto,
  MidtransBillPayRequestDto,
} from '../dto';
import { IBillPayReq, IBillPayRes } from '../entity';
import { IBillPayRepository } from '../repository';

export class MidtransBillPayService implements IBillPayRepository {
  constructor(private httpClient: IHttpClient) {}

  async createTransaction(data: IBillPayReq): Promise<IBillPayRes> {
    const { orderId, amount } = data;
    const response = await this.httpClient.post<
      IMidtransBillPayRequestDto,
      IMidtransBillPayResponseDto
    >({
      url: '/v2/charge',
      data: new MidtransBillPayRequestDto({
        payment_type: 'echannel',
        echannel: {
          bill_info1: 'Payment',
          bill_info2: `orderId: ${orderId}`,
        },
        transaction_details: {
          order_id: orderId,
          gross_amount: amount,
        },
      }),
    });

    const { bill_key, biller_code, transaction_status, order_id } = response;

    const x: IBillPayRes = {
      billKey: bill_key,
      billerCode: biller_code,
      transactionStatus: transaction_status,
      orderId: order_id,
    };

    return x;
  }
}
