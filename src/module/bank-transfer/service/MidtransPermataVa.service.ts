import { IHttpClient } from '../../http-client';
import { IMidtransPermataVaResponseDto, IMidtransVaRequestDto, MidtransVaRequestDto } from '../dto';
import { IVaReq, IVaRes } from '../entity';
import { IVaRepository } from '../repository';

export class MidtransPermataVaService implements IVaRepository {
  constructor(private httpClient: IHttpClient) {}

  async createTransaction(data: IVaReq): Promise<IVaRes> {
    const { bank, orderId, amount } = data;
    const response = await this.httpClient.post<
      IMidtransVaRequestDto,
      IMidtransPermataVaResponseDto
    >({
      url: '/v2/charge',
      data: new MidtransVaRequestDto({
        payment_type: 'permata',
        bank_transfer: {
          bank,
        },
        transaction_details: {
          order_id: orderId,
          gross_amount: amount,
        },
      }),
    });

    const { permata_va_number, transaction_status, order_id } = response;

    const x: IVaRes = {
      vaNumber: permata_va_number,
      transactionStatus: transaction_status,
      orderId: order_id,
    };

    return x;
  }
}
