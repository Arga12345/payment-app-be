import { IIdGenerator } from '../../common';
import { IBillPayRes } from '../entity';
import { IBillPayRepository } from '../repository';

export interface ICreateBillPayTransactionUC {
  execute(amount: number, userId: string): Promise<IBillPayRes>;
}

export class CreateBillPaymentTransaction implements ICreateBillPayTransactionUC {
  constructor(
    private repository: IBillPayRepository,
    private idGenerator: IIdGenerator,
  ) {}
  async execute(amount: number, userId: string): Promise<IBillPayRes> {
    const orderId = await this.idGenerator.generateUniqeId(userId);
    const response = await this.repository.createTransaction({
      orderId,
      amount,
    });

    return response;
  }
}
