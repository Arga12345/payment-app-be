import { IIdGenerator } from '../../common';
import { BankTransfer, IVaRes } from '../entity';
import { IVaRepository } from '../repository';

export interface ICreateVaTransactionUC {
  execute(bank: BankTransfer, amount: number, userId: string): Promise<IVaRes>;
}

export class CreateVaTransaction implements ICreateVaTransactionUC {
  constructor(
    private repository: IVaRepository,
    private idGenerator: IIdGenerator,
  ) {}
  async execute(bank: BankTransfer, amount: number, userId: string): Promise<IVaRes> {
    const orderId = await this.idGenerator.generateUniqeId(userId);
    const response = await this.repository.createTransaction({
      orderId,
      amount,
      bank,
    });

    return response;
  }
}
