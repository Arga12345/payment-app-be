export interface IMidtransError {
  status_code: string;
  status_message: string;
  id: string;
}
