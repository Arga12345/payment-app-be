export interface IAppError {
  message: string;
  code: number;
  correlationId?: string;
}

export class AppError implements IAppError {
  constructor(data: IAppError) {
    Object.assign(this, data);
  }
  message!: string;
  code!: number;
}

export interface IAppErrorUnprocessableEntity extends IAppError {
  validationError: string[];
}

export class AppErrorUnprocessableEntity implements IAppErrorUnprocessableEntity {
  constructor(
    public validationError: string[],
    public correlationId?: string,
  ) {}
  message: string = 'Validation Error';
  code: number = 422;
}
