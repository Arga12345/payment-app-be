export interface IAppResponse<T> {
  code: number;
  data: T;
}

export class AppResponse<T> implements IAppResponse<T> {
  constructor(data: IAppResponse<T>) {
    Object.assign(this, data);
  }
  code!: number;
  data!: T;
}
