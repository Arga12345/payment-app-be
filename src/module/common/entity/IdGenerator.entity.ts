import { AppError } from './AppError.entity';

export interface IIdGenerator {
  generateUniqeId(uniqeKey: string): Promise<string>;
}

export class NativeIdGenerator implements IIdGenerator {
  generateUniqeId(uniqeKey: string): Promise<string> {
    return new Promise((resolve, reject) => {
      try {
        const randomNumber = Math.floor(Math.random() * 9000) + 1000;
        const timestamp = new Date().getTime();
        const orderId = `${uniqeKey}${timestamp}${randomNumber}`;
        resolve(orderId);
      } catch (error) {
        reject(
          new AppError({
            message: `Internal server error, ${(error as Error).message}`,
            code: 500,
          }),
        );
      }
    });
  }
}
