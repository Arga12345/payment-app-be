import { AppErrorUnprocessableEntity, IAppErrorUnprocessableEntity } from './AppError.entity';

export interface IObjectValidator {
  validateObject(): Promise<IAppErrorUnprocessableEntity>;
}

export class ObjectValidator implements IObjectValidator {
  private obj: { [key: string]: string | number | boolean | null };
  private schema: { [key: string]: string };

  constructor(
    obj: { [key: string]: string | number | boolean | null },
    schema: { [key: string]: string },
  ) {
    this.obj = obj;
    this.schema = schema;
  }

  async validateObject(): Promise<IAppErrorUnprocessableEntity> {
    const errors: string[] = [];

    for (const key in this.schema) {
      const validationType = this.schema[key];
      const propertyValue = this.obj[key];

      if (
        validationType === 'required' &&
        (propertyValue === undefined || propertyValue === null || propertyValue === '')
      ) {
        errors.push(`${key} is required but is missing or null.`);
      }

      if (validationType === 'number' && typeof propertyValue !== 'number') {
        errors.push(`${key} must be a number.`);
      }

      if (validationType === 'boolean' && typeof propertyValue !== 'boolean') {
        errors.push(`${key} must be a boolean.`);
      }

      if (validationType === 'string' && typeof propertyValue !== 'string') {
        errors.push(`${key} must be a string.`);
      }
    }

    return new AppErrorUnprocessableEntity(errors);
  }
}
