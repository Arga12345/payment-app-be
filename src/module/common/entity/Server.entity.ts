export interface IServer {
  configureMiddleware(): void;
  configureRoutes(): void;
  startApp(): void;
}
