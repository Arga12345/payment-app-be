import { AppResponse, IAppResponse } from '../../common';
import { IEwalletRequestDto, IEwalletResponseDto } from '../dto';
import { ICreateEwalletTransactionUC } from '../usecase';

export interface IEwalletController {
  createTransaction(data: IEwalletRequestDto): Promise<IAppResponse<IEwalletResponseDto>>;
}

export class EwalletController implements IEwalletController {
  constructor(private x: ICreateEwalletTransactionUC) {}
  async createTransaction(data: IEwalletRequestDto): Promise<IAppResponse<IEwalletResponseDto>> {
    const { amount, userId, ewallet } = data;
    const response = await this.x.execute(amount, ewallet, userId);
    return new AppResponse({
      code: 201,
      data: response,
    });
  }
}
