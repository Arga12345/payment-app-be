import { EWallet } from '../entity';

export interface IEwalletResponseDto {
  url: string;
}

export interface IEwalletRequestDto {
  amount: number;
  userId: string;
  ewallet: EWallet;
}
