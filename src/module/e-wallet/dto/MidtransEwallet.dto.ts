interface IMidtransEwalletTransactionResponseDto {
  status_code: string;
  status_message: string;
  transaction_id: string;
  order_id: string;
  merchant_id: string;
  gross_amount: string;
  currency: string;
  payment_type: string;
  transaction_time: string;
  transaction_status: string;
  fraud_status: string;
  actions: Action[];
}

interface Action {
  name: string;
  method: string;
  url: string;
}

export interface IMidtransQrisTransactionResponseDto
  extends IMidtransEwalletTransactionResponseDto {
  acquirer: string;
}

export interface IMidtransGopayTransactionResponseDto
  extends IMidtransEwalletTransactionResponseDto {
  channel_response_code: string;
  channel_response_message: string;
}

export interface IMidtransShopeeTransactionResponseDto
  extends IMidtransGopayTransactionResponseDto {}
