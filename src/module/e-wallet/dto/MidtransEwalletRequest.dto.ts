import { EWallet } from '../entity';

interface ITransactionDetails {
  order_id: string;
  gross_amount: number;
}

interface IMidtransEwalletRequestDto {
  payment_type: EWallet;
  transaction_details: ITransactionDetails;
}

export interface IMidtransShopeeDeepLinkRequestDto extends IMidtransEwalletRequestDto {
  shopeepay?: {
    callback_url: string;
  };
}

export interface IMidtransGopayDeepLinkRequestDto extends IMidtransEwalletRequestDto {
  gopay?: {
    enable_callback: boolean;
    callback_url: string;
  };
}

export interface IMidtransQrisRequestDto extends IMidtransEwalletRequestDto {
  qris?: {
    acquirer: EWallet;
  };
}
