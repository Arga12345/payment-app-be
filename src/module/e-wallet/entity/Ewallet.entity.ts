export type EWallet = 'gopay' | 'shopeepay';

export interface IEwalletTrnsactionResponse {
  url: string;
}

export interface IEWalletTransactionRequest {
  amount: number;
  orderId: string;
  ewallet: EWallet;
}
