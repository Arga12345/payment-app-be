import { IEWalletTransactionRequest, IEwalletTrnsactionResponse } from '../entity';

export interface IEWalletRepository {
  createTransaction(req: IEWalletTransactionRequest): Promise<IEwalletTrnsactionResponse>;
}
