import { AppError } from '../../common';
import { IHttpClient } from '../../http-client';
import {
  IMidtransGopayDeepLinkRequestDto,
  IMidtransGopayTransactionResponseDto,
  IMidtransShopeeDeepLinkRequestDto,
} from '../dto';
import { IEWalletTransactionRequest, IEwalletTrnsactionResponse } from '../entity';
import { IEWalletRepository } from '../repository';

export class MidtransEwalletDeepLinkTransaction implements IEWalletRepository {
  constructor(private x: IHttpClient) {}
  async createTransaction(req: IEWalletTransactionRequest): Promise<IEwalletTrnsactionResponse> {
    const { amount, orderId, ewallet } = req;
    const data: IMidtransShopeeDeepLinkRequestDto | IMidtransGopayDeepLinkRequestDto = {
      payment_type: ewallet,
      transaction_details: {
        gross_amount: amount,
        order_id: orderId,
      },
    };
    const response = await this.x.post<
      IMidtransShopeeDeepLinkRequestDto | IMidtransGopayDeepLinkRequestDto,
      IMidtransGopayTransactionResponseDto
    >({
      url: '/v2/charge',
      data,
    });
    const { actions } = response;
    const url = actions.find((el) => el.name === 'deeplink-redirect')?.url;
    if (url) {
      return {
        url,
      };
    } else {
      throw new AppError({
        message: 'Internal server error',
        code: 500,
      });
    }
  }
}
