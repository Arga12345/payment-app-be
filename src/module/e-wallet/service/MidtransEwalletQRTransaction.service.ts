import { AppError } from '../../common';
import { IHttpClient } from '../../http-client';
import { IMidtransGopayTransactionResponseDto, IMidtransQrisRequestDto } from '../dto';
import { IEWalletTransactionRequest, IEwalletTrnsactionResponse } from '../entity';
import { IEWalletRepository } from '../repository';

export class MidtransEwalletQrisTransaction implements IEWalletRepository {
  constructor(private x: IHttpClient) {}
  async createTransaction(req: IEWalletTransactionRequest): Promise<IEwalletTrnsactionResponse> {
    const { amount, orderId, ewallet } = req;
    const data: IMidtransQrisRequestDto = {
      payment_type: ewallet,
      transaction_details: {
        gross_amount: amount,
        order_id: orderId,
      },
      qris: {
        acquirer: ewallet,
      },
    };
    const response = await this.x.post<
      IMidtransQrisRequestDto,
      IMidtransGopayTransactionResponseDto
    >({
      url: '/v2/charge',
      data,
    });
    const { actions } = response;
    const url = actions.find((el) => el.name === 'qris-redirect')?.url;
    if (url) {
      return {
        url,
      };
    } else {
      throw new AppError({
        message: 'Internal server error',
        code: 500,
      });
    }
  }
}
