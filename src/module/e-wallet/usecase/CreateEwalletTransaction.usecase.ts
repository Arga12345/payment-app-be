import { IIdGenerator } from '../../common';
import { EWallet, IEwalletTrnsactionResponse } from '../entity';
import { IEWalletRepository } from '../repository';

export interface ICreateEwalletTransactionUC {
  execute(amount: number, ewallet: EWallet, userId: string): Promise<IEwalletTrnsactionResponse>;
}

export class CreateEwalletTransaction implements ICreateEwalletTransactionUC {
  constructor(
    private x: IEWalletRepository,
    private y: IIdGenerator,
  ) {}

  async execute(
    amount: number,
    ewallet: EWallet,
    userId: string,
  ): Promise<IEwalletTrnsactionResponse> {
    const orderId = await this.y.generateUniqeId(userId);
    const response = await this.x.createTransaction({
      amount,
      ewallet,
      orderId,
    });
    return response;
  }
}
