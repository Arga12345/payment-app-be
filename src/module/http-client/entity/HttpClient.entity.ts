import { IHttpRequestConfig } from './HttpRequestConfig.entity';

export interface IHttpClient {
  get<T>(requestConfig: IHttpRequestConfig<void>): Promise<T>;
  post<X, T>(requestConfig: IHttpRequestConfig<X>): Promise<T>;
  put<X, T>(requestConfig: IHttpRequestConfig<X>): Promise<T>;
  delete<T>(requestConfig: IHttpRequestConfig<void>): Promise<T>;
}
