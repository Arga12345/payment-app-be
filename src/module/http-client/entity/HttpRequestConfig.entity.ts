export type IResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';

type CommonRequestHeadersList =
  | 'Accept'
  | 'Content-Length'
  | 'User-Agent'
  | 'Content-Encoding'
  | 'Authorization';

export type HeadersValue = string | string[] | number | boolean | null;

export type HttpClientHeaders = Partial<
  {
    [Key in CommonRequestHeadersList]: HeadersValue;
  } & { 'Content-Type': ContentType }
>;

type ContentType =
  | HeadersValue
  | 'text/html'
  | 'text/plain'
  | 'multipart/form-data'
  | 'application/json'
  | 'application/x-www-form-urlencoded'
  | 'application/octet-stream';

export interface IHttpRequestConfig<D> {
  url: string;
  data?: D;
  responseType?: IResponseType;
  headers?: HttpClientHeaders;
}
